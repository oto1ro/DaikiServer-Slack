package xyz.mc_daiki.plugins.slack;

import xyz.mc_daiki.plugins.web.WebRequest;

public class SlackPoster {

	public static void sendMessage(String _url, String msg) {
		sendMessage(_url, msg, "Information", ":server:");
	}

	public static void sendMessage(String _url, String msg, String name) {
		sendMessage(_url, msg, name, ":server:", "#logs");
	}

	public static void sendMessage(String _url, String msg, String name, String channel) {
		sendMessage(_url, msg, name, ":server:", channel);
	}

	public static void sendMessage(String _url, String msg, String name, String emoji, String channel) {
		WebRequest.post(_url,
				"payload=" + "{ \"text\": \"" + msg
				+ "\", \"username\": \"" + name
				+ "\", \"icon_emoji\": \"" + emoji
				+ "\", \"channel\": \"" + channel + "\" }"
		);
	}

}
