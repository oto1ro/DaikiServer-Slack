package xyz.mc_daiki.plugins.slack;

import org.bukkit.plugin.java.JavaPlugin;

public class DaikiServerSlack extends JavaPlugin {

	private static DaikiServerSlack plugin;
	public static String _url = "https://hooks.slack.com/services/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";

	@Override
    public void onEnable() {
		plugin = this;

		getServer().getPluginManager().registerEvents(new EventListener(), this);

		SlackPoster.sendMessage(DaikiServerSlack._url, "DaikiServer-Slackが有効になりました。",   "[有効]");
    	SlackPoster.sendMessage(DaikiServerSlack._url, "DaikiServer-Slackが有効になりました。",   "[Enable]", "#general");
	}

	@Override
	public void onDisable() {
		SlackPoster.sendMessage(DaikiServerSlack._url, "DaikiServer-Slackが無効になりました。",   "[無効]");
    	SlackPoster.sendMessage(DaikiServerSlack._url, "DaikiServer-Slackが無効になりました。",   "[Desable]", "#general");
	}

	public static DaikiServerSlack getPlugin() {
		return plugin;
	}

}
