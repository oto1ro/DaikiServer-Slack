package xyz.mc_daiki.plugins.slack;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EventListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
    	SlackPoster.sendMessage(DaikiServerSlack._url, "[チャット]" +event.getPlayer().getName(), event.getMessage());
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
    	SlackPoster.sendMessage(DaikiServerSlack._url, event.getPlayer().getName() + "さんがサーバに接続しました。",   "[参加]");
    	SlackPoster.sendMessage(DaikiServerSlack._url, event.getPlayer().getName() + "さんがサーバに接続しました。",   "[ログイン]", "#general");
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
    	SlackPoster.sendMessage(DaikiServerSlack._url, event.getPlayer().getName() + "さんがサーバから切断しました。", "[退出]");
    	SlackPoster.sendMessage(DaikiServerSlack._url, event.getPlayer().getName() + "さんがサーバから切断しました。", "[ログアウト]", "#general");
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
    	SlackPoster.sendMessage(DaikiServerSlack._url, event.getMessage(), "[コマンド]" + event.getPlayer().getName());
    }

}
